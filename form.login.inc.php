<?php if(!defined('PLX_ROOT')) exit;
/**
 * Plugin adhesion
 * $this IS adhesion : used with finclude L_NEED_AUTH (ART,TAG,CAT,PAGE,ANNUARY, ... LOCKED)
 * @version	2.3.3
 * @date	05/08/2020
 * @author	Stephane F, Cyril MAGUIRE, Thomas Ingles
 **/
#TODO capcha?
$hookId = '';
if($this->loginLogout){#v2.2.2 loginLogout hook set $this->loginLogout for always show it
	if($this->form_login_adherent){#preserve js funk doFocus('idpwadh') called in lang file : search : L_NEED_AUTH
		$hookId = '-'.$this->loginLogoutCount;
	}
	$this->form_login_adherent = $this->plxMotor->mode;
}elseif(!$this->form_login_adherent){
	$this->form_login_adherent = $this->plxMotor->mode;
}else{
	return;//Only one form in page!
}
$this->loginLogoutCount++;
?>
<div class="<?= $this->plxMotor->mode.($class?' '.$class:''); ?>" id="espace-adherents<?= $hookId ?>">
	<h3 class="widget-title icon-parents"><?php
	echo $this->getParam('mnuMembers');
	#echo' : '. $this->plxMotor->mode.' : '. $this->plxMotor->cible.' : '.$this->plxMotor->path_url.' : '.$this->form_login_adherent.' : '.time()
	?> - <?php
	echo $this->getParam('mnuConnexion') ?></h3>
<?php
if (isset($_SESSION['timeoutAdh']) ) {
	#unset($_SESSION['maxtryAdh'],$_SESSION['timeoutAdh']);var_dump(time(),$_SESSION['timeoutAdh'],$_SESSION['maxtryAdh']);// to debug
	if (time() < $_SESSION['timeoutAdh'] ) {
?>
	<p style="text-align:center;"><?php $this->lang('L_PLUGIN_MAXTRY')?>&nbsp;<?= date('H\hi',(60+$_SESSION['timeoutAdh']))?></p>
	<p id="forgetmypass"><a href="<?= $this->plxMotor->urlRewrite('?forgetmypass.html');?>"><?php $this->lang('L_FORGET_PASS')?></a></p>
</div>
<?php return;
	}#FI time() < $_SESSION['timeoutAdh']
}#FI isset($_SESSION['timeoutAdh']
?>
	<form action="" method="post">
		<fieldset>
			<p>
				<label for="idpwadh<?= $hookId ?>"><?php $this->lang('L_ID') ?>&nbsp;:</label><br class="hide" />
				<input type="text" name="login" maxlength="150" value="" id="idpwadh<?= $hookId ?>" required />
			</p><p>
				<label for="pwadh<?= $hookId ?>"><?php $this->lang('L_FORM_PASSWORD') ?>&nbsp;:</label><br class="hide" />
				<input type="password" name="password" size="25" maxlength="50" value="" id="pwadh<?= $hookId ?>" required />
				<input type="hidden" name="lockArticles" />
				<input type="hidden" name="adMotorId" value="<?= $this->plxMotor->path_url; ?>" />
			</p>
			<p><input type="submit" value="<?php $this->lang('L_FORM_OK') ?>" /></p>
		</fieldset>
		<p class="wall-e">
			<label for="walle<?= $hookId ?>"><?php $this->lang('L_FORM_WALLE') ?></label>
			<input id="walle<?= $hookId ?>" name="wall-e" type="text" size="50" value="<?= plxUtils::strCheck(@$_POST['wall-e']) ?>" maxlength="50" />
		</p>
	</form>
	<p id="new-adherent"><a href="<?= $this->plxMotor->urlRewrite('?adhesion.html');?>"><?= $this->getParam('mnuAdhesion')?></a></p>
	<p id="forgetmypass"><a href="<?= $this->plxMotor->urlRewrite('?forgetmypass.html');?>"><?= $this->getParam('mnuForgetMyPass') ?></a></p>
</div>
<?php if($this->loginLogoutCount > 1) return; ?>
<script>function doFocus(i){window.location.hash='espace-adherents';window.document.getElementById(i).focus();}</script>