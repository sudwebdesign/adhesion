<?php if(!defined('PLX_ROOT')) exit;
/**
 * Plugin adhesion
 * $this IS plxShow object
 * @version	2.3.3
 * @date	05/08/2020
 * @author	Stephane F, Cyril MAGUIRE, Thomas Ingles
 **/
$getOut = false;
if(
(!isset($_SESSION['lockArticles']['articles']) && !isset($_SESSION['lockArticles']['categorie']) && !isset($_SESSION['account']))
OR($_SESSION['lockArticles']['articles'] != 'on' && $_SESSION['lockArticles']['categorie'] != 'on' && $_SESSION['account'] != '')
) {#n'est pas connecté
	$getOut = true;
}
if(!isset($_SESSION['account'])) {#n'est pas connecté
	$getOut = true;
}
if($getOut){#Si l'utilisateur n'est pas connecté,
	header('Location:'.$this->plxMotor->urlRewrite('?login-page.html'));#on le redirige vers la page de connexion
	exit;
}
unset($getOut);
$useCapcha = ($this->plxMotor->aConf['capcha'] AND !!$this->plxMotor->plxPlugins->aPlugins['adhesion']->getParam('capcha'));//$plxPlugin->getParam('capcha');//num2bool
include('form.init.inc.php');#init plug & capcha
$plxPlugin->getAdherents();
$verif = substr($_SESSION['account'],5,-3);
$compte = array(NULL);
foreach ($plxPlugin->plxRecord_adherents->result as $key => $account) {
	if (md5($account['mail']) == $verif) {
		$compte = $plxPlugin->plxRecord_adherents->result[$key];
		break;
	}
}

if($compte == array(NULL)) {
	//On supprime les index de session
	unset($_SESSION['lockArticles']);
	unset($_SESSION['account']);
	unset($_SESSION['domainAd']);
	header('Location:'.$this->plxMotor->urlRewrite());
	exit;
}
//Définition de variables
$pro = array();
$error = array();
$success=false;
$wall_e = '';
$pwMinLen = $plxPlugin->getParam('pwMinLen');
if(!empty($_POST) && !empty($_POST['wall-e'])) {
	$wall_e = $_POST['wall-e'];
}
if(!empty($_POST) && empty($wall_e)) {

	if(!empty($_POST['password'])){//IF is chng pass btn (clicked)

		$amePw = FALSE;
		$id = $plxPlugin->adherentsList[$_SESSION[$pluginName]];
		$sel = $plxPlugin->plxRecord_adherents->result[$id]['salt'];
		$rand1 = $plxPlugin->plxRecord_adherents->result[$id]['rand1'];
		$rand2 = $plxPlugin->plxRecord_adherents->result[$id]['rand2'];
		$cle = $plxPlugin->plxRecord_adherents->result[$id]['cle'];
		if($cle.$rand1 != '00my'){# NO Manual password defined
			if(is_numeric($rand1)){#pw before v2.2.2
				$passAuto = substr($plxPlugin->plxRecord_adherents->result[$id]['mail'],0,-$rand1);#Automatic Password (cut mail)
			}else{
				$passAuto = $rand1;#v2.2.2 Automatic password
			}
			$amePw = (trim($_POST['password1']) == $cle.'-'.$passAuto.$rand2);#si le même
		}
		if(trim($_POST['password1'])=='' OR strlen(trim($_POST['password1']))<$pwMinLen OR trim($_POST['password1'])!=trim($_POST['password2']) OR $amePw){
			if($amePw) $_SESSION['pw'.get_class($plxPlugin)] = true;
			$_SESSION['lockArticles']['error'] .= $plxPlugin->getLang('L_EDIT_PASS_KO'.($amePw?'_SAME':'')).'<br />';
		}else{
			$password = $plxPlugin->defPassword($_SESSION[$pluginName], $_POST['password1']);//edit 4 manual Password
			$plxPlugin->recAdherentsList(false, $_SESSION[$pluginName]);
			$_SESSION['lockArticles']['success'] .= $plxPlugin->getLang('L_EDIT_PASS_OK').'<br />';
		}
		header('Location:'.$this->plxMotor->path_url);//.$this->plxMotor->racine.$this->plxMotor->get
		exit;
	}
//Prepare editMyAccount()
	$compte['nom']=strtoupper(trim(plxUtils::strCheck($_POST['nom'])));
	$compte['prenom']=strtolower(trim(plxUtils::strCheck($_POST['prenom'])));
	$compte['coordonnees']=plxUtils::strCheck($_POST['coordonnees']);
	if(trim($compte['coordonnees']) == '')
		$error[] = $plxPlugin->getLang('L_ERR_COORDONNEES');
	if ($plxPlugin->getParam('typeAnnuaire') == 'professionnel') {
		$compte['activite'] = plxUtils::strCheck($_POST['activite']);
		$compte['activite_autre']=trim(plxUtils::strCheck($_POST['activite_autre']));
		$compte['etablissement']=trim(plxUtils::strCheck($_POST['etablissement']));
		$compte['service']=trim(plxUtils::strCheck($_POST['service']));
		$compte['tel_office']=str_replace(array('.','-',' ','_','+','(',')',',',':',';','/'),'',plxUtils::strCheck($_POST['tel_office']));
		if($compte['activite'] =='')
			$error[] = $plxPlugin->getLang('L_ERR_ACTIVITE');
		if($compte['activite'] =='autre' && trim($compte['activite_autre']) == '')
			$error[] = $plxPlugin->getLang('L_ERR_AUTRE_ACTIVITE');
		if(trim($compte['etablissement']) == '')
			$error[] = $plxPlugin->getLang('L_ERR_ETABLISSEMENT');
		if(trim($compte['service']) == '')
			$error[] = $plxPlugin->getLang('L_ERR_SERVICE');
		if(trim($compte['tel_office']) != '' && !preg_match('!^[0-9]{3,13}[0-9]?$!',$compte['tel_office']))
			$error[] = $plxPlugin->getLang('L_ERR_TEL_OFFICE');
	}

	$compte['adresse1']=trim(plxUtils::strCheck($_POST['adresse1']));
	$compte['adresse2']=trim(plxUtils::strCheck($_POST['adresse2']));
	$compte['cp']=$_POST['cp'];
	$compte['ville']=trim(plxUtils::strCheck($_POST['ville']));
	$compte['tel']=str_replace(array('.','-',' ','_','+','(',')',',',':',';','/'),'',$_POST['tel']);
	$compte['mail']=trim(str_replace('&#64;', '@', plxUtils::strCheck($_POST['mail'])));
	$mailR=trim(str_replace('&#64;', '@', plxUtils::strCheck($_POST['mailR'])));#Retype (if modified)
	$compte['choix']=empty($_POST['choix']) ? 'adhesion': plxUtils::strCheck($_POST['choix']);
	$compte['mailing']=plxUtils::strCheck($_POST['mailing']);
	if(trim($compte['nom'])=='')
		$error[] = $plxPlugin->getLang('L_ERR_NAME');
	if(trim($compte['prenom'])=='')
		$error[] = $plxPlugin->getLang('L_ERR_FIRST_NAME');
	if(trim($compte['adresse1']) == '')
		$error[] = $plxPlugin->getLang('L_ERR_ADRESSE');
	if(trim($compte['cp']) == '' || strlen($compte['cp']) != 5 || !is_numeric($compte['cp']))
		$error[] = $plxPlugin->getLang('L_ERR_CP');
	if(trim($compte['ville']) == '')
		$error[] = $plxPlugin->getLang('L_ERR_VILLE');
	if(trim($compte['tel']) == '' || !preg_match('!^[0-9]{9,13}[0-9]$!',$compte['tel']))
		$error[] = $plxPlugin->getLang('L_ERR_TEL');
	if(trim($compte['mailing']) == '')
		$error[] = $plxPlugin->getLang('L_ERR_MAILING');
	if(!plxUtils::checkMail($compte['mail']))
		$error[] = $plxPlugin->getLang('L_ERR_MAIL');
	if(!plxUtils::checkMail($mailR))
		$error[] = $plxPlugin->getLang('L_ERR_MAILR');#Retype
	if($compte['mail'] != $mailR)
		$error[] = $plxPlugin->getLang('L_ERR_MAILS');#Sames?

	if($useCapcha){
		if (isset($this->plxMotor->plxPlugins->aPlugins['plxCapchaImage']))//capchaImage (module ok)
			$_SESSION['capcha']=sha1(@$_SESSION['capcha']);
		if($this->plxMotor->aConf['capcha'] AND $_SESSION['capcha'] != sha1($_POST['rep']))
			$error[] = $plxPlugin->getLang('L_ERR_ANTISPAM');
	}

	if(empty($error)) {
		if ($plxPlugin->editMyAccount($compte,$compte['id'])) {//On édite le compte de l'adhérent
			if($compte['choix'] == 'stop') {//Si l'utilisateur ne souhaite plus être membre de l'asso, on envoie une notification à un admin
				$content = $plxPlugin->notification($compte['nom'],$compte['prenom'],$compte['adresse1'],$compte['adresse2'],$compte['cp'],$compte['ville'],$compte['tel'],$compte['mail'],$compte['choix'],$compte['mailing']);
				if($plxPlugin->sendEmail($plxPlugin->getParam('nom_asso'),$plxPlugin->getParam('email'),$plxPlugin->getParam('email'),$plxPlugin->getParam('devalidation_subject'),$content,'html')){
						$_SESSION['lockArticles']['success'] .= $plxPlugin->getLang('L_EDIT_OK').'<br/>'.$plxPlugin->getLang('L_FORM_ERASE_OK').'<br />';
						unset($_SESSION['account']);
						unset($_SESSION['isConnected']);
						unset($_SESSION['lockArticles']);
						header('Location:'.$this->plxMotor->urlRewrite());
						exit;
				}
			}
			if (!isset($_SESSION['retrievePass'])){#in change email adress #editMyAccount()
				$_SESSION['lockArticles']['success'] .= $plxPlugin->getLang('L_EDIT_OK').'<br />';
			}
		} else {
			$error = array($plxPlugin->getLang('L_INTERNAL_ERR'));
		}
		header('Location:'.$this->plxMotor->path_url);//.$this->plxMotor->racine.$this->plxMotor->get
		exit;
	}
}
?>
<style>#form_adherer p#courrielP{display:none}</style>
<noscript><style>#form_adherer p#courrielP{display:block}</style></noscript>
<div id="form_adherer">
<?php if(!empty($error)): ?>
		<div class="contact_error">
<?php if(!empty($error)): ?>
				<h3><?php $plxPlugin->lang('L_FORM_FIELDS_MISSING') ?></h3>
				<ul>
<?php foreach ($error as $e) {
					echo PHP_EOL.'						<li>'.$e.'</li>';
				}
?>
				</ul>
<?php endif; ?>
		</div>
	<?php endif;
	unset($_POST);?>

	<p id="all_required"><?= sprintf($plxPlugin->getLang('L_FORM_ALL_REQUIRED'),'<exp class="mandatory">*</exp>');?></p>
	<form action="#form" method="post" name="monadhesion">

<?php if($useCapcha)://$this->plxMotor->aConf['capcha']$this->lang('ANTISPAM_WARNING')?>
		<fieldset>
			<p>
				<label for="id_rep"><strong><?php $plxPlugin->lang('L_FORM_ANTISPAM') ?>&nbsp;:</strong></label>
			</p>
			<?php $this->capchaQ(); ?>
			<input id="id_rep" name="rep" type="text" size="2" maxlength="1" autocomplete="off" style="width: auto; display: inline;" required placeholder="<?php $plxPlugin->lang('L_FORM_REQUIRED'); ?>" />
		</fieldset>
<?php endif; ?>

<?php if(!isset($_SESSION['pw'.get_class($plxPlugin)])):#hide if is PassAuto ?>

		<fieldset><legend><h2><?php $plxPlugin->lang('L_FORM_IDENTITY');?><exp class="mandatory">*</exp>&nbsp;:</h2></legend>
			<p>
				<label for="name"><?php $plxPlugin->lang('L_FORM_NAME') ?><exp class="mandatory">*</exp>&nbsp;:</label>
				<input id="name" name="nom" type="text" size="30" pattern="[^0-9]+" value="<?= plxUtils::strCheck($compte['nom']) ?>" placeholder="<?php $plxPlugin->lang('L_FORM_REQUIRED'); ?>" maxlength="50" required />
			</p>
			<p>
				<label for="firstname"><?php $plxPlugin->lang('L_FORM_FIRST_NAME') ?><exp class="mandatory">*</exp>&nbsp;:</label>
				<input id="firstname" name="prenom" type="text" size="30" pattern="[^0-9]+" value="<?= plxUtils::strCheck($compte['prenom']) ?>" placeholder="<?php $plxPlugin->lang('L_FORM_REQUIRED'); ?>" maxlength="50" required />
			</p>
		</fieldset>
<?php if($plxPlugin->getParam('typeAnnuaire') == 'professionnel') :
 $aA = explode(',',$plxPlugin->getParam('tabActivites'));
 $aK = array_map('strtolower', $aA);
 $aActivites = array_combine($aK, $aA);
 $activite = strtolower($compte['activite']);
 $tot=count($aActivites);
 $ia=0;
?>
		<fieldset><legend><h2><?php $plxPlugin->lang('L_FORM_ACTIVITY');?><exp class="mandatory">*</exp>&nbsp;:</h2></legend>
<?php foreach($aActivites AS $iActi => $acti):
		echo ++$ia==$tot?'<br/>':'';
		if (!array_key_exists($activite,$aActivites)) {
			$activite = 'autre';
			$activite_autre = isset($compte['activite_autre'])?$compte['activite_autre']:$compte['activite'];
		} else {
			$activite_autre = '';
		}
		$compte['activite'] = isset($compte['activite_autre'])?$compte['activite_autre']:$compte['activite'];//if posted
?>
		<p class="act">
			<input id="<?= $iActi ?>" name="activite" type="radio" value="<?= $iActi ?>" <?= $activite == $iActi? 'checked="checked"' : ''; ?> required />
			<label for="<?= $iActi ?>"><?= $acti ?></label>
		</p>
<?php endforeach;?>
		<p>
			<label class="mask" for="activite_autre"><?php $plxPlugin->lang('L_FORM_DETAIL');?><exp class="mandatory">*</exp>&nbsp;:</label>
			<input class="mask" id="activite_autre" name="activite_autre" type="text" value="<?= $activite == 'autre' ? plxUtils::strCheck($compte['activite']) : ''; ?>" placeholder="<?php $plxPlugin->lang('L_FORM_REQUIRED'); ?>" />
		</p>
		</fieldset>
<?php endif; ?>
		<fieldset><legend><h2><?php $plxPlugin->lang('L_FORM_AGENDA');?><exp class="mandatory">*</exp>&nbsp;:</h2></legend>
<?php if($plxPlugin->getParam('typeAnnuaire') == 'professionnel') : ?>
		<p>
			<label for="etablissement"><?php $plxPlugin->lang('L_FORM_SOCIETY') ?><exp class="mandatory">*</exp>&nbsp;:</label>
			<input id="etablissement" name="etablissement" type="text" size="50" value="<?= plxUtils::strCheck($compte['etablissement']) ?>" placeholder="<?php $plxPlugin->lang('L_FORM_REQUIRED'); ?>" maxlength="50" required />
		</p>
		<p>
			<label for="service"><?php $plxPlugin->lang('L_FORM_SERVICE') ?><exp class="mandatory">*</exp>&nbsp;:</label>
			<input id="service" name="service" type="text" size="50" value="<?= plxUtils::strCheck($compte['service']) ?>" placeholder="<?php $plxPlugin->lang('L_FORM_REQUIRED'); ?>" maxlength="50" required />
		</p>
<?php endif; ?>
		<p>
			<label for="adresse1"><?php $plxPlugin->lang('L_FORM_ADDRESS') ?><exp class="mandatory">*</exp>&nbsp;:</label>
			<input class="xl" id="adresse1" name="adresse1" type="text" size="50" value="<?= plxUtils::strCheck($compte['adresse1']) ?>" placeholder="<?php $plxPlugin->lang('L_FORM_REQUIRED'); ?>" maxlength="50" required />
			<input class="xl" id="adresse2" name="adresse2" type="text" size="50" value="<?= plxUtils::strCheck($compte['adresse2']) ?>" placeholder="<?php $plxPlugin->lang('L_FORM_OPTIONAL'); ?>" maxlength="50" />
		</p>
		<p>
			<label for="cp"><?php $plxPlugin->lang('L_FORM_ZIP_CODE') ?><exp class="mandatory">*</exp>&nbsp;:</label>
			<input id="cp" name="cp" type="text" size="5" value="<?= plxUtils::strCheck($compte['cp']) ?>" placeholder="<?php $plxPlugin->lang('L_FORM_REQUIRED'); ?>" maxlength="5" required />
		</p>
		<p>
			<label for="ville"><?php $plxPlugin->lang('L_FORM_CITY') ?><exp class="mandatory">*</exp>&nbsp;:</label>
			<input id="ville" name="ville" type="text" size="50" value="<?= plxUtils::strCheck($compte['ville']) ?>" placeholder="<?php $plxPlugin->lang('L_FORM_REQUIRED'); ?>" maxlength="50" required />
		</p>
		<p>
			<label for="tel"><?php $plxPlugin->lang('L_FORM_TEL') ?><exp class="mandatory">*</exp>&nbsp;:</label>
			<input id="tel" name="tel" type="text" size="50" value="<?= plxUtils::strCheck($compte['tel']) ?>" placeholder="<?php $plxPlugin->lang('L_FORM_REQUIRED'); ?>" maxlength="50" required />
		</p>
<?php if($plxPlugin->getParam('typeAnnuaire') == 'professionnel') : ?>
		<p>
			<label for="tel_office"><?php $plxPlugin->lang('L_FORM_TEL_OFFICE') ?><exp class="mandatory">*</exp>&nbsp;:</label>
			<input id="tel_office" name="tel_office" type="text" size="50" value="<?= plxUtils::strCheck($compte['tel_office']) ?>" placeholder="<?php $plxPlugin->lang('L_FORM_REQUIRED'); ?>" maxlength="50" required />
		</p>
<?php endif; ?>
		<p>
			<label for="courriel"><?php $plxPlugin->lang('L_FORM_MAIL') ?><exp class="mandatory">*</exp>&nbsp;:</label>
			<input id="courriel" name="mail" type="email" size="255" value="<?= ($compte['mail'] != '')? str_replace('@','&#64;',$compte['mail']):''; ?>" required placeholder="<?php $plxPlugin->lang('L_FORM_MAIL_PH') ?> <?php $plxPlugin->lang('L_FORM_REQUIRED'); ?>" oninput="mailSame(this)" />
		</p>
		<p id="courrielP">
			<label for="courrielR"><?php $plxPlugin->lang('L_FORM_MAILR') ?><exp class="mandatory">*</exp>&nbsp;:</label>
			<input id="courrielR" name="mailR" type="email" size="255" value="<?= ($compte['mail'] != '')? str_replace('@','&#64;',$compte['mail']):''; ?>" required placeholder="<?php $plxPlugin->lang('L_FORM_MAILR_PH') ?> <?php $plxPlugin->lang('L_FORM_REQUIRED'); ?>" oninput="mailSame(this)" />
<?php printf($plxPlugin->getLang('L_FORM_MAIL_TIPS'), $plxPlugin->getParam('mnuConnexion'), $plxPlugin->getParam('mnuForgetMyPass'));?>
		</p>
		</fieldset>
		<fieldset><legend><h2><?php $plxPlugin->lang('L_FORM_CHOICE');?>&nbsp;:</h2></legend>
<?php if($plxPlugin->getParam('annee') != 'illimite'): ?>
		<p>
			<input id="renouveler" name="choix" type="radio" value="renouveler" <?= plxUtils::strCheck($compte['choix']) != 'stop'? 'checked="checked"' : ''; ?> />
			<label for="renouveler"><?php $plxPlugin->lang('L_FORM_RULES_RE') ?> <?= $plxPlugin->getParam('nom_asso'); ?></label>
		</p>
<?php endif;#$plxPlugin->getParam('annee') != 'illimite') ?>
		<p>
			<input id="stop" name="choix" type="radio" value="stop" <?= plxUtils::strCheck($compte['choix']) == 'stop'? 'checked="checked"' : ''; ?> />
			<label for="stop"><?php $plxPlugin->lang('L_FORM_RULES_NO') ?> <?= $plxPlugin->getParam('nom_asso'); ?></label>
		</p>
		</fieldset>
<?php if($plxPlugin->getParam('showAnnuaire') == 'on') : ?>
		<fieldset><legend><h2><?php $plxPlugin->lang('L_FORM_SHARING');?><exp class="mandatory">*</exp>&nbsp;:</h2></legend>
<?php if($plxPlugin->getParam('publicAnnuaire') == 'on') : ?>
		<p>
			<input id="public" name="coordonnees" type="radio" value="public" <?= plxUtils::strCheck($compte['coordonnees']) == 'public' ? 'checked="checked"' : ''; ?> required />
			<label for="public"><?php $plxPlugin->lang('L_FORM_RULES_SHARE_PUBLIC') ?> <?= $plxPlugin->getParam('nom_asso'); ?></label>
		</p>
<?php endif;#($plxPlugin->getParam('publicAnnuaire') == 'on') ?>
		<p>
			<input id="rec" name="coordonnees" type="radio" value="rec" <?= $compte['coordonnees'] == 'rec' ? 'checked="checked"' : ''; ?> required />
			<label for="rec"><?php $plxPlugin->lang('L_FORM_RULES_SHARE_MEMBER') ?> <?= $plxPlugin->getParam('nom_asso'); ?></label>
		</p>
		<p>
			<input id="refus" name="coordonnees" type="radio" value="refus" <?= $compte['coordonnees'] == 'refus' ? 'checked="checked"' : ''; ?> required />
			<label for="refus"><?php $plxPlugin->lang('L_FORM_RULES_SHARE_NO') ?> <?= $plxPlugin->getParam('nom_asso'); ?></label>
		</p>
		</fieldset>
<?php else:#fi ($plxPlugin->getParam('showAnnuaire') == 'on') ?>
			<input id="annuoff" name="coordonnees" type="hidden" value="<?= isset($compte['coordonnees'])?plxUtils::strCheck($compte['coordonnees']):'refus'; ?>" />
<?php endif; ?>

		<fieldset><legend><h2><?php $plxPlugin->lang('L_FORM_MAILING');?><exp class="mandatory">*</exp>&nbsp;:</h2></legend>
		<p>
			<input id="maillist" name="mailing" type="radio" value="maillist" <?= plxUtils::strCheck($compte['mailing']) == 'maillist' ? 'checked="checked"' : ''; ?> required />
			<label for="maillist"><?php $plxPlugin->lang('L_FORM_RULES_NEWS') ?> <?= $plxPlugin->getParam('nom_asso'); ?></label>
		</p>
		<p>
			<input id="blacklist" name="mailing" type="radio" value="blacklist" <?= plxUtils::strCheck($compte['mailing']) == 'blacklist' ? 'checked="checked"' : ''; ?> required />
			<label for="blacklist"><?php $plxPlugin->lang('L_FORM_RULES_NEWS_NO') ?></label>
		</p>
		<p class="wall-e">
			<label for="walle"><?php $plxPlugin->lang('L_FORM_WALLE') ?></label>
			<input id="walle" name="wall-e" type="text" size="50" value="<?= plxUtils::strCheck($wall_e) ?>" maxlength="50" />
		</p>

<?php if($plxPlugin->getParam('annee') != 'illimite') echo '		<p class="cotisation info">'.$plxPlugin->getLang('L_COTIS'). ' ' .$plxPlugin->cotisationAJour($compte['date']).'</p>'; ?>

<?php eval($this->plxMotor->plxPlugins->callHook('myAccountForm'.get_class($plxPlugin)));# Hook Plugins ?>

		<p class="text-right">
			<input type="submit" name="profil" value="<?php $plxPlugin->lang('L_FORM_BTN_MAJ') ?>" />
		</p>
		</fieldset>

<?php endif;#!isset($_SESSION['pw'... ?>

		<script>var pwPower = '<?php $plxPlugin->lang('L_PWASTRENGTH_S') ?>'.split(',');</script>
		<fieldset id="title_pw">
			<sup class="float-right"><?php $plxPlugin->lang('L_ACCOUNT_PASSWORD_TIPS') ?></sup>
			<legend><h2><?php $plxPlugin->lang('L_ACCOUNT_CHANGE_PASSWORD') ?><?php if(isset($_SESSION['pw'.get_class($plxPlugin)])) : ?><exp class="mandatory">*</exp><?php endif; ?>&nbsp;:</h2></legend>
			<p>
				<label for="login"><?php $plxPlugin->lang('L_ACCOUNT_LOGIN') ?>&nbsp;:</label>
				<sup class="float-right"><?php $plxPlugin->lang('L_ACCOUNT_LOGIN_TIPS') ?></sup>
				<?php plxUtils::printInput('login', str_replace(array('-','_'),'',plxUtils::title2url(strtolower($compte['nom'].$compte['prenom']))), 'text', '20-100', true) ?>
			</p>
			<p>
			<b style="float:right;clear:both;" id="id_password1_strenght" title="Force du mot de passe">&nbsp;</b><br />
				<label for="id_password1"><?php $plxPlugin->lang('L_ACCOUNT_PASSWORD') ?><exp class="mandatory">*</exp>&nbsp;:</label>
				<?php plxUtils::printInput('password1', '', 'password', '20-255', false, 'password" pattern=".{'.$pwMinLen.',}" autocomplete="new-password" placeholder="'.$pwMinLen.' '.$plxPlugin->getLang('L_ACCOUNT_PASSWORD_PLACEHOLDER').'" onkeyup="pwAStrength(this.id, pwPower)" onblur="pwAStrength(this.id, pwPower)') ?>
			</p>
			<p><b style="float:right;line-height:0;clear:both;" id="id_passwords_same" title="Les mots de passe sont ils les mêmes">&nbsp;</b><br />
				<label for="id_password2"><?php $plxPlugin->lang('L_ACCOUNT_CONFIRM_PASSWORD') ?><exp class="mandatory">*</exp>&nbsp;:</label>
				<?php plxUtils::printInput('password2', '', 'password', '20-255', false, 'password" pattern=".{'.$pwMinLen.',}" autocomplete="new-password" placeholder="'.$pwMinLen.' '.$plxPlugin->getLang('L_ACCOUNT_PASSWORD_PLACEHOLDER')) ?>
			</p>
			<p class="text-right">
				<input type="submit" id="password" name="password" value="<?php $plxPlugin->lang('L_ACCOUNT_UPDATE_PASSWORD') ?>" disabled="disabled" />
			</p>
		</fieldset>
	</form>

	<script src="<?= PLX_PLUGINS.$pluginName ?>/js/pwTools.js?v=<?= $plxPlugin::v ?>"></script>
	<script type="text/javascript">
		/* <![CDATA[ */
//#v2.2.2 : mdp & mel
		pwb0 = document.getElementById("password");
		pwsa = document.getElementById("id_passwords_same");
		pwa1 = document.getElementById("id_password1");
		pwa2 = document.getElementById("id_password2");
		function pwIsSame() {
			pwb0.disabled = true;
			pwa1.required = pwa2.required = false;
			if(pwa1.value || pwa2.value){
				pwa1.required = pwa2.required = true;
				pwb0.disabled = false;
			}
			pwsa.innerHTML = '<span style="color:' + (pwa1.value != pwa2.value? 'red">≠': 'green">=') + '</span>';
		}

		for(var i = 1; i < 3; i++){//event listener
			document.getElementById("id_password"+i).addEventListener("change", event => {
				pwIsSame();
			});
			document.getElementById("id_password"+i).addEventListener("blur", event => {
				pwIsSame();
			});
			document.getElementById("id_password"+i).addEventListener("keyup", event => {
				pwIsSame();
				if(event.key !== "Enter") return;// Use `.key` instead. : if push enter key in pass inputs : Make sure this code gets executed after the DOM|Elements is loaded. :insp: stackoverflow.com/a/45650898
				pwb0.click();// change pass button.
				event.preventDefault();// No need to `return false;`.
			});
		}

		function mailSame() {
			var courriel = document.getElementById('courriel');
			var courrielR = document.getElementById('courrielR');
			if (courriel.value != courrielR.value) {
				courriel.setCustomValidity('<?php $plxPlugin->lang('L_FORM_JS_MAILS_MISMATCH') ?>');
				courrielR.setCustomValidity('<?php $plxPlugin->lang('L_FORM_JS_MAILS_MISMATCH') ?>');
				courrielR.parentNode.style.display = 'block';
			} else {// inputs is valid -- reset the error message
				courriel.setCustomValidity('');
				courrielR.setCustomValidity('');
				if(courriel.value == '<?= $compte['mail'] ?>')
					courrielR.parentNode.style.display = 'none';
			}
		}

		document.getElementById('courriel').onchange = function() {
			var mail2 = document.getElementById('courrielR')
			var mail2p = document.getElementById('courrielP')
			if(this.value != '<?= $compte['mail'] ?>'){
				mail2.required = true;
				mail2p.style.display = 'block';
			}else{
				mail2p.style.display = 'none';
				mail2.value = this.value;
				mail2.required = false;
				mailSame();
			}
		}
//FI #v2.2.2 : mdp & mel
		document.getElementById('stop').onclick = function() {
			if(this.checked);
				if (!confirm("<?= plxUtils::strCheck($plxPlugin->getLang('L_FORM_CONFIRM')); ?>"))
					this.checked = false;
		}
<?php if($plxPlugin->getParam('typeAnnuaire') == 'professionnel') :#ici on cache ou affiche l'input autre :insp: http://stackoverflow.com/a/8997289 ?>
		var rad = document.monadhesion.activite;
		var othr = document.getElementsByClassName('mask');
		for(var i = 0; i < rad.length; i++) {
			rad[i].onclick = function() {
				if(this.value == 'autre'){//on affiche
					for(var m = 0;m < othr.length; m++)
						othr[m].style.display='';
					document.getElementById('activite_autre').required = true;
					document.getElementById('activite_autre').focus();
				}else{//on cache
					for(var m = 0;m < othr.length; m++)
						othr[m].style.display='none';
					document.getElementById('activite_autre').required = false;
					document.getElementById('activite_autre').value = '';
				}
			};
			var NoOth = (rad[i].value == 'autre' && rad[i].checked)?false:true;
		}
		if(NoOth)
			for(var m = 0;m < othr.length; m++)
				othr[m].style.display='none';
<?php endif; ?>
		/* ]]> */
	</script>
</div>
