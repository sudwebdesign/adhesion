<?php if(!defined('PLX_ROOT')) exit;
/**
 * Plugin adhesion login form
 * $this IS plxShow :
 * @version	2.3.3
 * @date	05/08/2020
 * @author	Thomas Ingles
 **/
$plxPlugin = $this->plxMotor->plxPlugins->aPlugins['adhesion'];
$plxPlugin->finclude(PLX_PLUGINS.get_class($plxPlugin).'/form.log'.(isset($_SESSION['account'])?'out':'in').'.inc.php',true,'htmlclass');