<?php if(!defined('PLX_ROOT')) exit;
/**
 * Plugin adhesion
 * $this IS adhesion : used with finclude : Members Space
 * @version	2.3.3
 * @date	05/08/2020
 * @author	Thomas Ingles
 **/
if(!$this->form_login_adherent)
	$this->form_login_adherent = $this->plxMotor->mode;
else
	return;//Only one form!
$pluginName = get_class($this);//adhesion
//$ad = $this->adherentsList[$id];
$compte = $this->plxRecord_adherents->result[$this->adherentsList[$_SESSION['adhesion']]];
?>
<div class="<?= $this->plxMotor->mode.($class?' '.$class:''); ?>" id="espace-adherents">
	<h3 class="widget-title icon-parents"><?= $this->getParam('mnuMembers');#echo '#test ::: mode: '. $this->plxMotor->mode.', get: '. $this->plxMotor->get.', cible: '. $this->plxMotor->cible.', path_url: '.$this->plxMotor->path_url.', form_login_adherent.: '.$this->form_login_adherent.', time: '.time() ?></h3>
<?php eval($this->plxMotor->plxPlugins->callHook('logout'.$pluginName)); # Hook plugins ?>
	<ul class="<?= $pluginName ?>-list unstyled-list">
		<li>
			<form action="" method="post" id="logout">
				<fieldset>
					<input type="hidden" name="logout">
					<input type="submit" value="<?= plxUtils::strCheck($this->getParam('mnuDeconnexion')) ?>" id="logout-sub"/>
				</fieldset>
<?php eval($this->plxMotor->plxPlugins->callHook('logoutForm'.$pluginName)); # Hook plugins ?>
			</form>
		</li>
<?php if($this->getParam('showAnnuaire') != 'no') : $annuName = $this->getParam('mnuAnnuaire'); ?>
		<li id="annuaire"><a href="<?= $this->plxMotor->urlRewrite('?annuaire.html') ?>" title="<?= plxUtils::strCheck($annuName) ?>"><?= $annuName ?></a></li>
<?php endif; ?>
<?php eval($this->plxMotor->plxPlugins->callHook('logoutList'.$pluginName)); # Hook plugins ?>
		<li id="myaccount"><a href="<?= $this->plxMotor->urlRewrite('?myaccount.html');?>" title="<?php $this->lang('L_MY_ACCOUNT') ?>"><?= $this->getParam('mnuMyAccount') ?></a></li>
	</ul>
<?php eval($this->plxMotor->plxPlugins->callHook('logoutEnd'.$pluginName)); # Hook plugins ?>
<?php
if($this->plxMotor->mode == 'login-page' AND $this->getParam('annee') != 'illimite'){
	echo '<p class="cotisation">'.$this->getLang('L_COTIS'). ' ' .$this->cotisationAJour($compte['date']).'</p>';
}
?>
</div>