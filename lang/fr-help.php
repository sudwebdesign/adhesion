<?php  if (!defined('PLX_ROOT')) exit;
$plxPlugin = $plxAdmin->plxPlugins->aPlugins['adhesion'];
$pluginName = get_class($plxPlugin); ?>
 <p class="in-action-bar">Aide du plugin Adhésion</p>
<?php if($_SESSION['profil']==PROFIL_ADMIN) { ?>
<h2>Espaces dédiés</h2>
<h5>Pour afficher le(s) formulaire(s) d'identification (espace adhérent(e)s)<br />
Veuillez dupliquer le code ci-dessous dans votre fichier de thème <b class="text-green" title="de préférence">sidebar.php</b>.</h5>
<code class="alert green">&lt;?php eval($plxShow->callHook('loginLogout')); ?&gt;</code>
<h5><i>Ou tout autre endroit où vous souhaitez le faire apparaitre.</i></h5>
<h2>Notes du réglage des cotisations</h2>
<h5>Lorsque le paramètre <b class="text-green">&laquo;<?php $plxPlugin->lang('L_ANNEE') ?>&raquo;</b> est sur réglé sur <b class="text-green">&laquo;<?php $plxPlugin->lang('L_ANNEE_ILLIMITE') ?>&raquo;</b><br />
le <b class="text-green">&laquo;<?php $plxPlugin->lang('L_VALIDATION_MESSAGE') ?>&raquo;</b> et le <b class="text-green">&laquo;<?php $plxPlugin->lang('L_DEVALIDATION_MESSAGE') ?>&raquo;</b><br />
ainsi que leurs objets seront les seuls envoyés si <b class="text-green">&laquo;<?php $plxPlugin->lang('L_LABEL_AUTO_VALID') ?>&raquo;</b> est activé*.<br />
<i>Il est probable que les mentions de paiements ne servent à rien pour ses deux cas là *(paramètre in/actif).</i></h5>
<h2>Aux Utilisateurs du plugin gutuma.</h2>
<h5>Pour rendre disponible la fonction d'infolettre a adhésion (liste de diffusion),<br />
vous devez créer une liste nommé: <b class="text-green">adherents</b> dans gutuma ;)</h5>
<h2>Aux Utilisateurs du plugin openStreetMap.</h2>
<h5>Pour rendre disponible le menu et la page localisation des adhérent(e)s (carte dynamique),<br />
vous pouvez régler :<br />
<pre>
Type de fichier à analyser : <b class="text-green">Code postal</b>
Item principal du fichier xml : <b class="text-green">adherent</b>
Item secondaire indiquant la ville : <b class="text-green">ville</b>
Item secondaire indiquant le code postal : <b class="text-green">cp</b>
</pre><br />
et pour les <b class="text-green">Items facultatifs (pop-up multiples)</b>, il suffit de suivre les  <b class="text-green">exple</b> :)
</h5>
<?php  if(defined('PLX_MYMULTILINGUE')) {# Si plugin plxMyMultilingue présent ?>
<div class="alert red">
<h2>/!\ Attention aux Utilisateur de plxMyMultilingue (0.8.1) /!\</h2>
<h3>Dans l'ordre des plugin: placer Adhésion <b class="text-green">avant</b> Multilingue. Voir en premier.<br /></h3>
<h4>Cela empêche le bon fonctionnement des espaces destinés aux adhérents coté admin.<br />
Perte de $id dans le hook AdminStatic() lorsque adhesion est chargé après.</h4>
<h4 class="alert green">Comme cela il est compatible Multilingue, et vos adhérents contents.</h4>
</div>
<?php  }#FI plxMyMultilingue présent ?>
<?php }#FI PROFIL_ADMIN ?>
<span id="liberapay" style="position:fixed;bottom:1em;right:1em;" title="<?php $plxPlugin->lang('L_LIBERAPAY');?>.">
<a href="https://liberapay.com/sudwebdesign/donate"><img alt="<?php $plxPlugin->lang('L_LIBERAPAY');?>." src="<?php echo PLX_PLUGINS.$pluginName.'/liberapay-fr.svg' ?>"></a>
</span>
