<?php

$LANG                              = array(

'L_ADMIN_MENU_NAME'                => 'Adhérents',#Nom du menu admin dans PluXml : admin & thème default
'L_ADMIN_TITLE_MENU'               => 'Gérer les inscrits du plugin adhesion',

'L_PAGE_TITLE'                     => 'Devenir Adhérent',#Devenir membre, Adhérer
'L_ID'                             => 'Identifiant',
'L_ADHESIONS'                      => 'Adhésions',
'L_DATE_FORMAT'                    => 'd/m/Y',
'L_LANG_ISO'                       => 'fr_FR',#pickadate trad
'L_FORMAT_SUBMIT'                  => 'dd/mm/yyyy',#pickadate yyyy/mm/dd
'L_FORMAT_TO_TXT'                  => 'dd/MM/yyyy',#pickadate yyyy/mm/dd
'L_ERROR_FILE_COPY'                => 'La copie du fichier <i>%</i> vers <i>%</i> a échoué...',
#print.php
'L_NO'                             => 'Non',
'L_YES'                            => 'Oui',
'L_FILE_NAME'                      => 'Liste_des_adherents_association_',
'L_WRONG FILE'                     => 'Mauvais Fichier tpl.',#Wrong file.
'L_FILE UNEXIST'                   => 'Fichier inexistant.',#File does not exist.
'L_PRINT_TITLE'                    => 'Liste des adhérents de l’association',
'L_DEACTIVATED'                    => 'Désactivées',#Annuaire
'L_INSCRIT'                        => 'Inscription validée',
# config.php
'L_MAIL_AVAILABLE'                 => 'Fonction d’envoi de mél disponible',
'L_MAIL_NOT_AVAILABLE'             => 'Fonction d’envoi de mél non disponible',
'L_ART_PROTECTED'                  => 'Article dédié aux inscrits',#protégé par mot de passe
'L_CAT_PROTECTED'                  => 'Catégorie dédiée aux inscrits',#protégée par mot de passe
'L_PGS_PROTECTED'                  => 'Page dédiée aux inscrits',#protégée par mot de passe
'L_CONFIG_ROOT_PATH'               => 'Emplacement du dossier des inscrits (fichiers xml)',
'L_CONFIG_ROOT_PATH_HELP'          => 'Ne pas oublier le slash (/) à la fin.',
'L_CAPCHA'                         => 'Activer le Capcha',
'L_CAPCHA_HELP'                    => 'Activer le Capcha sur la page : modifier le compte',
'L_LABEL_OPTREFUS'                 => 'Les choix &laquo;refus&raquo; Mél et Coordonnées en 1er (admin)',
'L_NBJTKNPW'                       => 'Jeton de R.A.Z. du M.D.P est valide pour (en jours)',
'L_NBJTKNPW_HELP'                  => 'Nombre de jours ou le lien de remise a zéro du mot de passe personnalisé est valide. Le jeton de RAZ expire après ce délais et devra être recréer.',
'L_NBJTKNPW_MAIL'                  => 'Ce mél est valable pour %s jours, vous pouvez le supprimer après avoir cliqué sur le lien.',
'L_PWMINLEN'                       => 'Longueur mini du mot de passe',
'L_PWMINLEN_HELP'                  => 'Nombre de caractères minimum acceptés pour les mots de passe personnalisés',
'L_MENU_DISPLAY'                   => 'Afficher le menu du plugin adhésion',
'L_MENU_STRCUTLOCKS'               => 'Tronquer les articles à partir de combiens de mots',
'L_MENU_STRCUTLOCKS_HELP'          => 'Permet d’afficher les N premiers mots des articles réservés aux inscrits. Laisser vide ou 0 pour désactiver.',
'L_MENU_TITLE'                     => 'Titre du menu principal',
'L_MENU_ADHERER_TITLE'             => 'Titre du menu des avantages de l’adhésion',
'L_MENU_ADHESION_TITLE'            => 'Titre du menu du formulaire d’adhésion',
'L_MENU_MEMBERS_TITLE'             => 'Titre du menu et des pages Espace adhérent',
'L_MENU_CONNEXION_TITLE'           => 'Titre du menu et des pages de connexion',
'L_MENU_DECONNEXION_TITLE'         => 'Titre du menu des liens de déconnexion',
'L_MENU_DESC'                      => 'Description de l’adhésion',
'L_MENU_FORGET_MY_PASS'            => 'Titre de la page de récupération du mot de passe',
'L_MENU_MY_ACCOUNT'                => 'Titre de la page du compte adhérent',
'L_MENU_ACCES_ANNUAIRE'            => 'Afficher l’annuaire public',
'L_ORDER'                          => 'Afficher les derniers adhérents en premier',
'L_SHOW_MENU_ANNUAIRE'             => 'Afficher l’annuaire',
'L_MENU_ANNUAIRE'                  => 'Titre de la page annuaire',
'L_ANNUAIRE_GEN'                   => 'Généraliste',
'L_ANNUAIRE_PRO'                   => 'Professionel',
'L_TYPE_ANNUAIRE'                  => 'Type d’annuaire',
'L_MENU_POS'                       => 'Position du menu',
'L_EMAIL'                          => 'Mél',
'L_EMAIL_SUBJECT'                  => 'Objet du mél',
'L_PASSWD_HASH_SUBJECT'            => 'Objet du mél pour la régénération du mot de passe',
'L_PASSWD_HASH_MESSAGE'            => 'Message pour la régénération du mot de passe',
'L_PASSWD_SUBJECT'                 => 'Objet du mél de récupération du mot de passe',
'L_PASSWD_MESSAGE'                 => 'Message de récupération du mot de passe',
'L_THANKYOU_MESSAGE'               => 'Message de remerciement',
'L_TEMPLATE'                       => 'Thème',
'L_SAVE'                           => 'Enregistrer',
'L_ADRESSE_ASSO'                   => 'Adresse de l’association',
'L_CLE'                            => 'Longueur de la clé des mots de passe générés',
'L_ANNEE'                          => 'Type d’année pour la durée de l’adhésion',
'L_ANNEE_CIVILE'                   => 'Année civile',
'L_ANNEE_ENTIERE'                  => 'Année entière',
'L_ANNEE_ILLIMITE'                 => 'Années illimitées',
'L_LABEL_AUTO_VALID'               => 'Les comptes se valident par mél',
'L_LABEL_AUTO_VALID_HELP'          => 'Peut s’activer si le type d’années est illimitées. Si ce parametre est activé, lorsqu’une personne a fait certifié son adresse de mél par le site, son compte se valide et son mot de passe temporaire lui est envoyé.',
'L_ANNEE_SS_COTIS'                 => 'Nombre d’années autorisés sans cotiser',
'L_ANNEE_SS_COTIS_HELP'            => 'Nombre d’années autorisés avant que l’adhérent(e) soit dévalidé(e) par le système. Inutilisé si le type d’années est illimitées.',
'L_RAPPEL_SUBJECT'                 => 'Objet du mél de rappel',
'L_RAPPEL_MESSAGE'                 => 'Message du mél de rappel',
'L_COTIS_SUBJECT'                  => 'Objet du mél de cotisation',
'L_COTIS_MESSAGE'                  => 'Message du mél de cotisation',
'L_VALIDATION_SUBJECT'             => 'Objet du mél de validation',
'L_VALIDATION_MESSAGE'             => 'Message du mél de validation',
'L_DEVALIDATION_SUBJECT'           => 'Objet du mél de dévalidation',
'L_DEVALIDATION_MESSAGE'           => 'Message du mél de dévalidation',
'L_NOM_ASSO'                       => 'Nom de l’association',
'L_DOMAINE_ASSO'                   => 'Nom de domaine du site (style&nbsp;asso.com)',
'L_DISPLAY'                        => 'Afficher',
'L_HIDE'                           => 'Masquer',
'L_ANNUAIRE'                       => 'Annuaire',

'L_DEFAULT_MENU_NAME'              => 'Adhérer',
'L_DEFAULT_MENU_ADHERER'           => 'Pourquoi Adhérer?',
'L_DEFAULT_MENU_ADHESION'          => 'Adhérer à l’association',
'L_DEFAULT_MENU_MEMBERS'           => 'Espace adhérents',
'L_DEFAULT_MENU_CONNEXION'         => 'Connexion',
'L_DEFAULT_MENU_DECONNEXION'       => 'Déconnexion',
'L_DEFAULT_DESC'                   => 'Placer ici ce que l’association peut apporter aux adhérents',
'L_DEFAULT_MENU_PASS'              => 'Récupération du mot de passe',
'L_DEFAULT_MENU_MY_ACCOUNT'        => 'Mon compte',
'L_DEFAULT_MENU_ANNUAIRE'          => 'Annuaire des adhèrents',
'L_DEFAULT_SUBJECT'                => '## NOM ASSOCIATON ## Nouvelle demande d’adhésion',
'L_DEFAULT_SUBJECT_HASH'           => '## NOM ASSOCIATON ## Recréation de votre mot de passe',
'L_DEFAULT_MSG_HASH'               => 'Vous avez demandé à régénérer votre mot de passe, Pour légitimer la demande, Cliquer sur le lien ci-dessous&nbsp;',
'L_DEFAULT_SUBJECT_PASS'           => '## NOM ASSOCIATON ## Récupération de votre mot de passe',
'L_DEFAULT_MSG_PASS'               => 'Vous avez demandé à récupérer votre sésame, ci-dessous se trouve votre mot de passe temporaire&nbsp;',
'L_DEFAULT_THANKYOU'               => 'Merci pour votre demande d’adhésion. Elle ne sera validée que lors de la réception de votre paiement (xxxx euros) par le trésorier de l’association.',
'L_DEFAULT_VAL_MSG'                => 'Merci pour votre adhésion. Nous venons de recevoir votre paiement et nous vous informons que vous êtes officiellement inscrit.',
'L_DEFAULT_VALIDATION_SUBJECT'     => '## NOM ASSOCIATON ## Demande d’adhésion validée',
'L_DEFAULT_COTIS_MSG'              => 'Merci pour votre ré-adhésion. Nous venons de recevoir votre paiement et nous vous informons que vous êtes officiellement ré-inscrit.',
'L_DEFAULT_COTIS_SUBJECT'          => '## NOM ASSOCIATON ## Cotisation annuelle reçu',
'L_DEFAULT_RAPPEL_MSG'             => 'Nous vous rappelons que nous n’avons pas encore reçu votre cotisation annuelle. Si vous souhaitez continuer votre adhésion, merci d’adresser votre règlement au trésorier de l’association.',
'L_DEFAULT_RAPPEL_SUBJECT'         => '## NOM ASSOCIATON ## Rappel de cotisation',
'L_DEFAULT_DEVAL_MSG'              => 'Nous vous informons que nous n’avons pas encore reçu votre cotisation annuelle. Si vous souhaitez continuer votre adhésion, merci d’adresser votre règlement au trésorier de l’association.',
'L_DEFAULT_DEVALIDATION_SUBJECT'   => '## NOM ASSOCIATON ## Demande de désinscription validée',
'L_WARNPRM_SUBJECT'                => 'Pensez à modifier l’objet du message d’adhésion',
'L_WARNPRM_DEVALIDATION_SUBJECT'   => 'Pensez à modifier l’objet du message de dévalidation',
'L_WARNPRM_VALIDATION_SUBJECT'     => 'Pensez à modifier l’objet du message de validation de l’adhésion',
'L_WARNPRM_COTIS_SUBJECT'          => 'Pensez à modifier l’objet du message de cotisation reçu',
'L_WARNPRM_RAPPEL_SUBJECT'         => 'Pensez à modifier l’objet du message de rappel de cotisation',
'L_WARNPRM_SUBJECT_PASSWORD_HASH'  => 'Pensez à modifier l’objet du message de régénération du mot de passe',
'L_WARNPRM_SUBJECT_PASSWORD'       => 'Pensez à modifier l’objet du message de récupération du mot de passe',
'L_WARNPRM_THANKYOU'               => 'Pensez à modifier le message de remerciement',
'L_WARNPRM_NOM_ASSO'               => 'Veuillez renseigner un nom d’association',
'L_WARNPRM_ADRESSE_ASSO'           => 'Veuillez renseigner une adresse pour l’association',
'L_WARNPRM_DESC_ADHESION'          => 'Veuillez expliquer ce que l’association apporte aux adherents',
'L_WARNPRM_NO_DIR_ADHES'           => 'Le dossier des adhésions n’a pas été créé. Vérifiez les droits en écriture et créez le manuellement pour que le plugin fonctionne.',

# bulletin adhesion ET myaccount
'L_VALIDMAIL_KO'                   => 'Lien déjà utilisé ou érroné.',#info : Clé de vérif ou adresse mél érronée
'L_VALIDMAIL_OK'                   => 'Merci, cette adresse mél est vérifiée.',
'L_VALIDMAIL_AND_ACCOUNT_OK'       => 'Merci, cette adresse mél validé. Votre mot de passe temporaire y est envoyé.',#. Vous pouvez maintenant vous connecter a votre espace
'L_PWASTRENGTH_S'                  => 'Très faible,Faible,Bon,Fort',#Forces des MDP : Very weak,Weak,Good,Strong

'L_REMOVE_THIS_MAIL'               => 'Merci de supprimer ce mél, il est utilisable une seule fois. Il est bien de l’éffacer de cette BAL si elle est en ligne (webmail) pour éviter qu’une âme malveillante y est accès plus tard.',#IDEA #BEP #UNUSED #PLM
'L_FORM_ALL_REQUIRED'              => 'Tous les champs marqués par %s sont requis',
'L_FORM_REQUIRED'                  => 'Obligatoire',
'L_FORM_OPTIONAL'                  => 'Facultatif',
'L_FORM_IDENTITY'                  => 'Identité',
'L_FORM_NAME'                      => 'Nom',
'L_FORM_FIRST_NAME'                => 'Prénom',
'L_FORM_MAIL'                      => 'Mél',
'L_FORM_MAIL_PH'                   => 'une.adresse@mel.valide.fr',
'L_FORM_MAILR'                     => 'Confirmer le mél',
'L_FORM_MAILR_PH'                  => 'Ressaisir la même adresse mél',
'L_FORM_JS_MAILS_MISMATCH'         => 'Veuillez faire correspondre les adresses mél',
'L_NOTI_MAIL'                      => 'Mél',
'L_FORM_OTHER'                     => 'Autre',
'L_FORM_DETAIL'                    => 'Précisez',
'L_FORM_CHOICE'                    => 'Votre choix',
'L_FORM_SHARING'                   => 'Partage de vos données',
'L_FORM_MAILING'                   => 'Des nouvelles de l’association',
'L_FORM_ACTIVITY'                  => 'Activité',
'L_FORM_AGENDA'                    => 'Coordonnées',
'L_FORM_SOCIETY'                   => 'Société',#Etablissement
'L_FORM_SERVICE'                   => 'Service',
'L_FORM_ADDRESS'                   => 'Adresse postale',
'L_FORM_ZIP_CODE'                  => 'Code Postal',
'L_FORM_CITY'                      => 'Ville',
'L_FORM_TEL'                       => 'Tél',#Téléphone
'L_FORM_TEL_OFFICE'                => 'Tél pro ou poste',
'L_FORM_BTN_SEND'                  => 'Envoyer',
'L_FORM_BTN_MAJ'                   => 'Mettre à jour',#Modifier
'L_FORM_ANTISPAM'                  => 'Vérification anti-spam',
'L_FORM_ERASE'                     => 'Votre demande de désinscription a bien été prise en compte. N’hésitez pas à nous rejoindre si vous changez d’avis',
'L_FORM_ERASE_OK'                  => 'Désinscription enregistrée.',
'L_FORM_ERASE_FORM_LIST_OK'        => 'Désinscription des listes de diffusion enregistrée.',
'L_ALREADY_REC'                    => 'Les modifications ont bien été prises en compte.',
'L_FORM_OK'                        => 'OK',
'L_FORGET_PASS'                    => 'Mot de passe oublié?',
'L_MY_ACCOUNT'                     => 'Paramétrer mon compte',
'L_ACCOUNT_LOGIN'                  => 'Mon Identifiant',
'L_ACCOUNT_LOGIN_TIPS'             => 'Pour modifier l’identifiant, changez le nom et le prénom de votre compte.',
'L_ACCOUNT_CHANGE_PASSWORD'        => 'Changer le mot de passe',
'L_ACCOUNT_PASSWORD_TIPS'          => 'Pour un mot de passe sécurisé, <a title="Voir méthode 4 de wikihow" href="https://fr.wikihow.com/cr%C3%A9er-un-mot-de-passe-facile-%C3%A0-retenir#step_1_4" target="_blank">créer une phrase facile a mémoriser</a>.',
'L_ACCOUNT_PASSWORD'               => 'Mot de passe',
'L_ACCOUNT_PASSWORD_PLACEHOLDER'   => 'caractères minimum',
'L_ACCOUNT_CONFIRM_PASSWORD'       => 'Confirmer le mot de passe',
'L_ACCOUNT_UPDATE_PASSWORD'        => 'Modifier le mot de passe',
'L_EMAIL_HASH_OK'                  => 'Le mél avec le lien pour générer votre nouveau mot de passe temporaire est envoyé.',
'L_EMAIL_HASH_KO'                  => 'Le mél pour générer votre nouveau mot de passe est déjà envoyé.<br />Cocher la case &laquo;Forcer&raquo; pour vous l’envoyer à nouveau.',
'L_EMAIL_PASS_OK'                  => 'Un mél vient de vous être envoyé avec votre mot de passe temporaire.',
'L_EMAIL_PASS_KO'                  => 'Une erreur est survenue. Merci de verifier le mél ou utiliser le formulaire de contact.',
'L_EDIT_PASS_OK'                   => 'Mot de passe modifié.',
'L_EDIT_PASS_KO'                   => 'Les &laquo;mots de passes&raquo; diffèrent ou sont trop courts.',
'L_EDIT_PASS_KO_SAME'              => 'Le mot de passe est celui définit par le site.',
'L_EDIT_MAIL_OK'                   => 'Mél modifié.<br />Nouveau mot de passe généré et envoyé a cette adresse&nbsp',
'L_EDIT_MAIL_KO'                   => 'Erreur d’envoi du mél avec le nouveau mot de passe, Aucune modification est enregistré!<br />Veuillez réitérer la demande dans quelques minutes et bien vérifier l’ortographe du mél&nbsp',
'L_EDIT_OK'                        => 'Les modifications sont enregistrées',
'L_DEVAL_OK'                       => 'Vous n’êtes plus membre de l’association',
'L_FORM_CONFIRM'                   => 'Êtes-vous sûr de ne plus vouloir être membre de l’association? Cette action est irréversible.',
'L_NEW_MAIL_PASS'                  => 'Vous venez de changer votre mél, voici votre nouveau mot de passe temporaire.',
'L_NEW_MAIL_PASS_SUBJECT'          => 'Nouvelle adresse, nouveau sésame',
'L_NEW_MAIL_PASS_REG'              => 'Un administrateur vous a généré un nouveau mot de passe temporaire. Vous pouvez maintenant vous connecté grâce à lui. Il se trouve ci-dessous',
'L_NEW_MAIL_PASS_SUBJECT_REG'      => 'Nouveau sésame temporaire',
'L_FORM_MAIL_TIPS'                 => #NOTABENE
			'<br /><br /><i><sup><b>Remarque</b>&nbsp;: Lorsque le mél est modifié, <b>vous serez déconnecté du site</b> et votre <b>nouveau mot de passe</b> sera envoyé a la nouvelle adresse (bien vérifier son orthographe*).
			<br /><b>N.B.</b>&nbsp;: Si le message se perd en route, est introuvable (Indésirables, SPAM, Pourriels, etc...), que la boite aux lettres est accessible et qu’elle a assez d’espace libre pour en recevoir de nouveaux.
			<br />Il y a en bas de la page "<b>%s</b>", le lien "<b>%s</b>" et dans le champ prévu de celle-ci, y saisir votre nouveau mél pour y recevoir votre nouveau sésame.
			<br />*Si l’adresse mél saisie est erronée, veuillez nous écrire via la page de contact, nous pourrons la modifier pour vous.</sup></i>',

#forgetmypass
'L_FORM_ID'                        => 'Identifiant numérique (option)',
'L_FORM_ID_TIPS'                   => 'Numéro d’inscrit, si connu, permet d’accélérer le processus. Laisser le champ vide en cas de doute.',
'L_FORM_STEP2'                     => 'Étape #2&nbsp;:<br />Valider la demande pour générer un nouveau sésame et l’envoyer par mél.',
'L_FORM_PW_CLOSE_TAB'              => 'Opération réalisée avec succés, vous pouvez maintenant fermer cet onglet ou l’utiliser pour surfer.',
'L_FORM_RESEND_HASH'               => 'Forcer pour envoyer une nouvelle fois le mél pour réinitialiser le mot de passe&nbsp;',
'L_FORM_RESEND_HASH_TIPS'          => 'Ce mél est envoyé lors de la première demande (réussie).<br />Par la suite, pour toutes les requêtes, un message va vous indiquer qu’il est déjà envoyé.<br />S’il est absent de votre boite réception et des indésirables (SPAM, Pourriels, ...) après plusieurs minutes,<br />cocher cette case force le site a vous le ré-expedier.',
'L_CHANGE_YOUR_PASS'               => 'Votre mot de passe est temporaire, modifier le pour retrouver un accès complet aux pages réservées.',

# erreurs
'L_WARNPRM_EMAIL'                  => 'Veuillez saisir une adresse mél valide',
'L_WARNPRM_ADHERENTS'              => 'Le chemin du dossier des fiches adhérents doit être complété.',
'L_WARNPRM_NO_GULIST'              => 'Liste de diffusion &quot;adherents&quot; indisponible. Veuillez vérifier sa présence dans le plugin ',#gutuma
'L_ERR_REC'                        => 'Les donnés n’ont pas été correctement enregistrées.',
'L_ERR_NAME'                       => 'Le nom',
'L_ERR_FIRST_NAME'                 => 'Le prénom',
'L_ERR_ACTIVITE'                   => 'L’activité',
'L_ERR_AUTRE_ACTIVITE'             => 'L’intitulé de votre activité',
'L_ERR_ETABLISSEMENT'              => 'L’établissement',
'L_ERR_SERVICE'                    => 'Le service',
'L_ERR_ADRESSE'                    => 'L’adresse',
'L_ERR_CP'                         => 'Le code postal',
'L_ERR_VILLE'                      => 'La ville',
'L_ERR_TEL'                        => 'Le téléphone',
'L_ERR_TEL_OFFICE'                 => 'Le téléphone professionel',
'L_ERR_CHOIX'                      => 'Votre choix',
'L_ERR_COORDONNEES'                => 'Le traitement de vos données',
'L_ERR_MAILING'                    => 'Recevoir des nouvelles de l’association',#AARCH
'L_ERR_MAIL'                       => 'Le premier mél',
'L_ERR_MAILR'                      => 'Le deuxieme mél',
'L_ERR_MAILS'                      => 'Les méls différent',#Le deuxieme mél (pour vérifier) est différent du premier
'L_ERR_MAILF'                      => 'Mél invalide',#forgetMypass
'L_ERR_ANTISPAM'                   => 'La vérification anti-spam a échoué',
'L_FORM_THANKYOU'                  => 'Merci pour votre demande.',
'L_MSG_WELCOME'                    => 'Merci de remplir le formulaire ci-dessous',
'L_ERR_SENDMAIL'                   => 'Une erreur est survenue pendant l’envoi de votre message',
'L_ERR_USER_ALREADY_USED'          => 'Ce compte existe déjà',
'L_ERR_USER_ALREADY_VALID'         => 'Votre compte est encore valide',
'L_ADMIN_ERR_USER_ALREADY_VALID'   => 'Ce compte est encore valide',#
'L_ERR_SENDEMAIL'                  => 'Echec d’envoi du mél de confirmation',
'L_ERR_PASS_SENT'                  => 'Echec d’envoi du mot de passe par mél à la personne',
'L_ERR_LOGIN_SENT'                 => 'Echec d’envoi de l’identifiant par mél à la personne',
'L_ERR_RAPPEL_SENT'                => 'Echec d’envoi du rappel à cotisation par mél à la personne',

'L_ERR_NO_GUTUMA'                  => 'Le plugin adhésion nécessite le plugin <a href=\"http://www.ecyseo.net/\" title=\"Voir page téléchargements\" onclick=\"window.open(this.href);return false;\">Gutuma</a> pour fonctionner. Ce dernier doit être activé.',
'L_INTERNAL_ERR'                   => 'Échec de l’enregistrement&nbsp;: erreur interne',

'L_FORM_WALLE'                     => 'Si vous souhaitez que votre demande ne soit jamais prise en compte, remplissez ce champ ^_^',
'L_FORM_RULES'                     => 'Je souhaite devenir adhérent de l’Association',
'L_FORM_RULES_RE'                  => 'Je souhaite renouveler mon adhésion',
'L_FORM_RULES_NO'                  => 'Je souhaite désadhérer de l’Association',
'L_FORM_RULES_SHARE_PUBLIC'        => 'J’accepte que mes coordonnées figurent sur l’annuaire public',
'L_FORM_RULES_SHARE_MEMBER'        => 'J’accepte que mes coordonnées figurent sur l’annuaire de l’espace privé',
'L_FORM_RULES_SHARE_NO'            => 'Je refuse que mes coordonnées figurent sur l’annuaire',#Je refuse que mes coordonnées figurent sur le site de l’Association
'L_FORM_RULES_NEWS'                => 'J’accepte de recevoir par mél toute information concernant le site',
'L_FORM_RULES_NEWS_NO'             => 'Je refuse de recevoir sur ma messagerie des informations concernant le site',
'L_NOTI_RULES_NEWS'                => 'Accepte de recevoir par messagerie toute information concernant le site',
'L_NOTI_RULES_NEWS_NO'             => 'Refuse de recevoir sur ma messagerie des informations concernant le site',
'L_NOTI_SENDING_DATE'              => 'Envoyé à l’association le',
'L_FORM_FIELDS_MISSING'            => 'Un ou plusieurs champs n’ont pas été convenablement complétés&nbsp;:',

'L_ADD_ADD'                        => 'Votre adresse (%s) est ajoutée à la liste de diffusion',#function editMyAccount
'L_REMOVE_ADD'                     => 'Votre Adresse (%s) est retirée de la liste de diffusion',#function editMyAccount

'L_COORD_PUBLIC'                   => 'Diffuse ses coordonnées au public',
'L_COORD_REC'                      => 'Diffuse ses coordonnées aux inscrits',
'L_COORD_NO'                       => 'Refuse de diffuser ses coordonnées',
'L_NEWS_OK'                        => 'Accepte les méls',
'L_NEWS_NO'                        => 'Refuse les méls',
'L_ADHESION_OK'                    => 'Souhaite devenir adhérent',
'L_ADHESION_STOP'                  => 'Souhaite arréter son adhésion',
'L_ADHESION_RENEW'                 => 'Souhaite renouveler son adhésion',
# admin
'L_TITLE_CONFIG'                   => 'Gérer les Fiches',
'L_ADMIN_LIST_MEMBER_NEW'          => 'Inscrire une nouvelle personne (À valider)',
'L_ADMIN_LIST_MEMBERS_TO_VALIDATE' => 'À valider',#Liste des adhérents
'L_ADMIN_LIST_MEMBERS_VALIDATED'   => 'Validés',#Liste des adhérents
'L_ADMIN_LIST_MEMBERS_VALIDE'      => 'Valides',#Liste des adhérents
'L_COTIS'                          => 'Cotisation',
'L_COTIS_ZERO'                     => 'N/A',#in funk cotisationAJour()
'L_COTIS_OK'                       => 'Valide pour',#in funk cotisationAJour()
'L_COTIS_NO'                       => 'En retard de',#in funk cotisationAJour()
'L_DAYS'                           => 'jour(s)',#in funk cotisationAJour()
'L_VALID'                          => 'Validé',#Mél (Non) Validé #alt & img title
'L_FIRST_DEMAND_DATE_TITLE'        => 'Date de la première demande',

'L_ONLY_LETTERS'                   => 'Seule les lettres sont admises',
'L_TIME'                           => 'Heure',
'L_NEW_TIME'                       => 'Nouvelle heure',
'L_DATE'                           => 'Date',
'L_NEW_DATE'                       => 'Nouvelle date',
'L_CHANGE_DATE_BTN'                => 'Changer la date',
'L_CHANGE_DATE'                    => 'd’adhésion',
'L_CHANGE_FIRST_DATE'              => 'de la première adhésion',
'L_PERSONNAL_PASS_SAVED'           => 'Mot de passe enregistré',
'L_PERSONNAL_PASS'                 => 'MDP Perso actif',
'L_PERSONNAL_PASS_PLACEHOLDER'     => 'Mot de Passe, min',#toggleDiv?

'L_ADMIN_COORDS_TIPS'              => 'L’étoile (*) signifie que ce réglage est désactivé.'.PHP_EOL.'Il est indisponible à la page &laquo;Mon compte&raquo;',

'L_ADMIN_EXPORT_LIST'              => 'Exporter',
'L_ADMIN_EXPORT_FORMAT'            => 'Exporter les données au format',
'L_ADMIN_LIST_ID'                  => 'ID',
'L_ADMIN_FIRST_ASK'                => 'Inscrit le',#Inscrit <br/>depuis le
'L_ADMIN_FIRST_SEL'                => 'Inscrit le ',#IN SELECT OPTION VALUE : NO BR INSIDE
'L_ADMIN_DATE_VAL'                 => 'Validé le',
'L_ADMIN_LIST_NAME'                => 'Nom',
'L_ADMIN_LIST_FIRST_NAME'          => 'Prénom',
'L_ADMIN_LIST_ACTIVITY'            => 'Activité',
'L_ADMIN_LIST_STRUCTURE'           => 'Société',#Etablissement
'L_ADMIN_LIST_DPT'                 => 'Service',
'L_ADMIN_LIST_ADRESSE'             => 'Adresse',
'L_ADMIN_LIST_ZIP_CODE'            => 'CP',#Code Postal
'L_ADMIN_LIST_CITY'                => 'Ville',
'L_ADMIN_LIST_TEL'                 => 'Tél',#Téléphone
'L_ADMIN_LIST_TEL_PRO'             => 'Tél pro',#fessionel
'L_ADMIN_LIST_TEL_OFFICE'          => 'Poste',
'L_ADMIN_LIST_MAIL'                => 'Mél',
'L_ADMIN_LIST_CHOICE'              => 'Choix',
'L_ADMIN_LIST_COORDONNEES'         => 'Annuaire',#Partage
'L_ADMIN_LIST_MAILING'             => 'Infolettres',
'L_ADMIN_LIST_VALIDATION'          => 'Valider',
'L_ADMIN_LIST_UPDATE'              => 'Mettre à jour',
'L_ADMIN_LIST_COTISE'              => 'A cotisé(e)',
'L_ADMIN_LIST_DEVALIDATION'        => 'Dévalider',
'L_ADMIN_LIST_REGENEREPASS'        => 'Regénérer le MDP',
'L_ADMIN_LIST_NEW'                 => 'Nouvelle adhésion',
'L_ADMIN_LIST_PW_GOOD'             => 'Mot de passe personnel. Non envoyable.',
'L_ADMIN_LIST_PW_LOST'             => 'Demande de regénération du mot de passe en cours...',
'L_ADMIN_APPLY_BUTTON'             => 'Enregistrer',
'L_ADMIN_SELECTION'                => 'Pour la sélection',
'L_ADMIN_DELETE'                   => 'Effacer',
'L_ADMIN_VALIDATION_PENDING'       => 'Aucune personne a valider',
'L_ADMIN_NO_VALIDATION'            => 'Aucune personne validée',
'L_ADMIN_ADD_ADD'                  => 'Liste de diffusion, ajout de l’adresse (%s) de %s.',
'L_ADMIN_NEW_PASS'                 => 'Nouveau mot de passe envoyé, l’adresse (%s) de %s changée.',
'L_ADMIN_NEW_PASS_REG'             => ' renénéré et envoyé à l’adresse (%s) de %s.',
'L_ADMIN_AUTO_PASS_VALID_ADH'      => 'Mot de passe temporaire de %s généré',
'L_ADMIN_PERSO_PASS_VALID_ADH'     => 'Mot de passe de %s modifié',
'L_ADMIN_REMOVE_ADD'               => 'Liste de diffusion, rejet de l’adresse (%s) de %s.',
'L_ADMIN_REMOVE_ADH'               => 'Fiche de %s supprimée',
'L_ADMIN_UPDATE_ADH'               => 'Fiche de %s modifiée',
'L_ADMIN_NEW_ADH'                  => 'Fiche de %s ajoutée',
'L_ADMIN_VALIDATION_ADH'           => 'Fiche de %s validée',
'L_ADMIN_DEVALIDATION_ADH'         => 'Fiche de %s désactivée',
'L_ADMIN_SEND_LOGIN'               => 'Envoyer le mél avec l’identifiant',
'L_ADMIN_SEND_RAPPEL'              => 'Envoyer le mél de rappel à cotisation',
'L_ADMIN_SEND_RAPPEL2'             => 'Envoyer le deuxième mél de rappel à cotisation',
'L_FORM_NEW'                       => 'Nouvelle <br class="sml-hide med-show"/>demande',
'L_FORM_OLD'                       => 'Ancien(ne) <br class="sml-hide med-show"/>adhérent(e)',
'L_ADMIN_DATE_DEL'                 => 'Dévalidé le',
'L_ADMIN_LIST_CLE'                 => 'Clef d’activation',
'L_ADMIN_PASSWORD'                 => 'Votre mot de passe temporaire est&nbsp;',
'L_ADMIN_ID'                       => 'Votre identifiant est',# votre nom relié à votre prénom, le tout en minuscules, sans espace, sans caractère accentué ou exotique comme les cédilles, les tirets, etc. (exemple&nbsp; dupondjeanfrancois).',####SIMPLIFY : YOUR ID IS : name... be carefull lang is edited (ol sel66)######
'L_ADMIN_CONFIRM'                  => 'Êtes-vous sûr de vouloir supprimer la fiche de cette personne? Cette action est irréversible!',
#'L_ADMIN_CHOICE_CONFIRM'           => 'Êtes-vous sûr de vouloir changer le statut de cet adhérent ?',
'L_ADMIN_NOT_DONE'                 => 'Non renseignée',
'L_TAB_ACTIVITIES'                 => 'Quelles sont les activités possible',
'L_TAB_ACTIVITIES_HELP'            => 'Pour l’annuaire pro. Le dernier index doit être &laquo;Autre&raquo;.',
'L_LABEL_FIND'                     => 'Filtrer les fiches',
'L_LABEL_DELETE_FILTER'            => 'Supprimer le filtre',
'L_PASS_SENT'                      => 'Mot de passe envoyé',
'L_LOGIN_SENT'                     => 'Identifiant envoyé',
'L_YOUR PASS_IS'                   => 'Votre mot de passe temporaire est',
'L_ADMIN_SEND_PASS'                => 'Renvoyer son mot de passe temporaire',
'L_PASS_IS_SENDED'                 => 'Mot de passe créé et envoyé',

'L_RAPPEL_1'                       => 'Premier',
'L_RAPPEL_2'                       => 'Deuxième',
'L_RAPPEL_SENT'                    => 'Mél de rappel à cotisation envoyé',

'L_ADMIN_ACTION'                   => 'Action',
'L_VALIDATION'                     => 'Validation',
'L_MODIFICATION'                   => 'Modification',
'L_SUPPRESSION'                    => 'Suppression',

# LookArticles
#'L_HIDE_LOCKED_ARTICLES'           => 'Masquer de la page d’accueil les articles dédiés aux adhérents',#unused
#'L_HIDE_LOCKED_CATEGORIES'         => 'Masquer de la page d’accueil les articles des catégories dédiées aux adhérents',#unused
'L_SAVE'                           => 'Sauvegarder',
'L_ARTICLE_PASSWORD_FIELD_LABEL'   => 'Cet article est dédié aux inscrits',
'L_PLUGIN_BAD_PASSWORD'            => 'Mauvais identifiant et/ou mot de passe.',
'L_PLUGIN_MAXTRY'                  => 'Nombre de tentatives dépassées. Vous pourrez tenter de vous reconnecter à partir de',
#'L_PLUGIN_EXPIRED_PASSWORD'        => 'Mot de passe expiré',
'L_CATEGORIE_PASSWORD_FIELD_LABEL' => 'Cette catégorie est dédiée aux inscrits',
'L_FORM_PASSWORD'                  => 'Mot de passe',
'L_FORM_ADMIN_PASSWORD'            => 'Cette page statique est dédiée aux inscrits',
'L_FORM_OK'                        => 'Entrer',
'L_CONNEXION_OK'                   => 'Vous êtes connecté(e)',
'L_DECONNEXION_OK'                 => 'Vous êtes déconnecté(e)',
'L_NEED_AUTH'                      => 'Réservé aux inscrit(e)s. <a href="javascript:" onclick="doFocus(\'idpwadh\');">%s</a> ou <a href="%s">%s</a> pour y accéder',#Vous devez d'abord vous connecter ou vous enregistrer pour annoncer un événement.
'L_NEED_AUTH_FEED'                 => 'Réservé aux inscrit(e)s',#Vous devez d'abord vous connecter ou vous enregistrer pour annoncer un événement.
#ADRESS /!\ never % for font-size or other style : sprintf()

'L_BJR_MSG'                        => 'Bonjour',# prenom nom.
'L_BJR_MSG_VALID'                  => 'Pour simplifier l’inscription et être sûr que cette adresse mél est ouverte, vous pouvez la valider en cliquant sur le lien suivant&nbsp;',
'L_BJR_MSG_VALID_AUTO'             => 'Pour activer votre compte et verifier que cette adresse mél est ouverte, vous pouvez cliquer sur le lien suivant&nbsp;',
'L_NOCOTIS_HTML'                   => "\r\n".'		<p>Merci de patienter le temps que votre inscription soit validée.</p>'."\r\n".'<p>Vous recevrez alors un mél avec votre mot de passe temporaire.</p>'."\r\n"/*.'<p>Vous pouvez nous envoyer un courrier à l’adresse postale suivante&nbsp;: </p>'."\r\n".'<p>Association <strong>%s</strong></p>'."\r\n".'		<div style="padding-left:50px;">%s</div>'."\r\n".'		<p>Ou avec le formulaire de contact du site.</p>'*/,
'L_ADRESS_HTML'                    => "\r\n".'		<p>Merci d’établir votre règlement à l’ordre de&nbsp;: Association <strong>%s</strong><br />et de le retourner à l’adresse postale suivante&nbsp;:</p>'."\r\n".'		<div style="padding-left:50px;">%s</div>'."\r\n".'		<p>Dès que votre inscription est enregistrée par le secrétariat de l’Association, vous recevrez alors la confirmation d’inscription par mél avec votre mot de passe.</p>',
'L_SPAM_HTML'                      => "\r\n".'		<p style="font-size:13px"><em>Si vous ne <strong>recevez aucun mél</strong>, pensez à vérifier régulièrement votre dossier <strong> Pourriels / Indésirrables / spams </strong> de votre messagerie.</em><br /></p>',

#CNIL
'L_CNIL_TEXT'                      => 'Pour vous désinscrire de la liste de diffusion, cliquez sur ou recopiez le lien ci-après dans le champ d’adresse de votre navigateur internet',
'L_CNIL_LINK'                      => 'Se désinscrire de la liste de diffusion',
'L_CNIL_NO_REPLY_TEXT'             => 'Merci de ne pas répondre à ce mél. Celui-ci ayant été généré automatiquement, personne ne va traiter votre réponse.'."\r\n"."\r\n".'Conformément à la Loi Informatique et Libertés du 06/01/1978 et à nos mentions légales vous disposez d’un droit d’accès et de rectification sur les données vous concernant.',

#class.adhesionMail
'L_CLASS_MAIL_ARRAY'               => array(
'checkMail' => "Class Mail, method Mail : Adresse Invalide",
'checkExp_ok_msg' => "Votre courriel, destiné à %s, a été envoyé!",
'checkExp_ok_sbj' => "Votre courriel au sujet de: %s...",
'checkExp_unknown' => " - [Adresse (%s) non reconnue!]",
'From_error' => "Class Mail: Erreur, From n'est pas de la bonne forme"
),//L_CLASS_MAIL_ARRAY default lang

#annuaire + admin (vanilla JS DataTable)
'L_LABEL_TABLE_PERPGS'             => 'Nombre de personnes à afficher par page (admin)',# fiches ?
'L_LABEL_JSDTABLE_PLACEHOLDE'      => 'Chercher...',#Search...
'L_LABEL_JSDTABLE_PERPGS'          => '{select} personnes par page',#{select} entries per page
'L_LABEL_JSDTABLE_NODATA'          => 'Aucune personne trouvée',#No entries found
'L_LABEL_JSDTABLE_INFO'            => 'Affichage de {start} à {end} sur {rows} personne(s)',#Showing {start} to {end} of {rows} entries
'L_JUNE'                           => 'Don en Ğ1, 1ere monnaie numérique libre',
);
