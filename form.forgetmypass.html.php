<?php if(!defined('PLX_ROOT')) exit;
/**
 * Plugin adhesion
 * $this IS plxShow
 * @version	2.3.3
 * @date	05/08/2020
 * @author	Stephane F, Cyril MAGUIRE, Thomas Ingles
 **/
$useCapcha = $this->plxMotor->aConf['capcha'];//TRUE;
include('form.init.inc.php');#init plug & capcha
$authRAZ = FALSE;
$email = $a = '';
if(!empty($_GET) AND !empty($_GET['email'])){#tep
	foreach(array('email','p','a','z') AS $v){#piece @jeton | @token, adherent id, time
		${$v} = '';#bep
		if(isset($_GET[$v]) AND !empty($_GET[$v])){
			${$v} = $_GET[$v];#bep
		}
	}
	if($p AND $a AND $z){
		if(is_numeric($a)){#aherent id
			$a = str_pad($a, 5, STR_PAD_LEFT);
			$id = $plxPlugin->adherentsList[$a];
			$sel = $plxPlugin->plxRecord_adherents->result[$id]['salt'];

			if($email == sha1($sel.$plxPlugin->plxRecord_adherents->result[$id]['mail'].$z.$plxPlugin->plxRecord_adherents->result[$id]['firstDate'])){#crypté
				$email = $plxPlugin->plxRecord_adherents->result[$id]['mail'];#remplace le mél
				if($plxPlugin->plxRecord_adherents->result[$id]['rand2'] !== '00'){#on verifie qu'une demande de RAZ est faite
					$authRAZ = TRUE;#Tester la validité du hash : autorisé
				}
			}
		}
	}
}
$email = plxUtils::checkMail($email)? $email: '';#bad mail remove it
#var_dump($authRAZ, $p, $a, $z);
echo (isset($_GET['close'])?'<h2 class="alert blue"><a href="'.$this->plxMotor->urlRewrite().'">'.$plxPlugin->getLang('L_FORM_PW_CLOSE_TAB').'</a></h2>':'');
#Vous êtes sur le point de demander de régénéré le mot de passe #idée
echo ($authRAZ? '<style>.step1{display:none;visibility:hidden;height:0px;}</style><h2 class="alert blue">'.$plxPlugin->getLang('L_FORM_STEP2').'</h2>': '');
?>
<form action="" method="post">
	<fieldset class="step1">
		<p><label><b><?php $plxPlugin->lang('L_FORM_MAIL') ?>&nbsp;:</b><br />
		<input type="email" name="email" size="255" value="<?= $email.(($authRAZ and $email)? '" readonly="readonly': '') ?>" required="required" placeholder="<?php $plxPlugin->lang('L_FORM_MAIL_PH') ?>" /></label></p>
		<p><label><b><?php $plxPlugin->lang('L_FORM_ID') ?>&nbsp;:</b><br /><sup><?php $plxPlugin->lang('L_FORM_ID_TIPS') ?></sup><br />
		<input type="number" name="a" size="5" value="<?= $a.($authRAZ? '" readonly="readonly': '') ?>" placeholder="123" /></label></p>
<?php if($authRAZ): ?>
		<input type="hidden" name="p" value="<?= $p ?>" />
		<input type="hidden" name="z" value="<?= $z ?>" />
<?php endif; ?>
<?php if($useCapcha): #$this->lang('ANTISPAM_WARNING')?>
	</fieldset>
	<fieldset>
		<p><label for="id_rep"><strong><?php $plxPlugin->lang('L_FORM_ANTISPAM') ?>&nbsp;:</strong></label></p>
		<?php $this->capchaQ(); ?>
		<input id="id_rep" name="rep" type="text" size="2" maxlength="1" autocomplete="off" style="width: auto; display: inline;" required />
<?php endif; ?>
		<p class="step1 step11"><label for="id_resendhash" class="alert green"><input id="id_resendhash" name="resendhash" type="checkbox" />&nbsp;<?php $plxPlugin->lang('L_FORM_RESEND_HASH') ?><br />
		<b>/!\</b><i><sup><br /><?php $plxPlugin->lang('L_FORM_RESEND_HASH_TIPS') ?></sup></i></label></p>
		<p><input type="submit" value="<?php $plxPlugin->lang('L_FORM_BTN_SEND') ?>" /></p>
		<input type="hidden" name="forgetmypass">
		<p class="wall-e">
			<label for="walle"><?php $plxPlugin->lang('L_FORM_WALLE') ?></label>
			<input id="walle" name="wall-e" type="text" size="50" value="" maxlength="50" />
		</p>
	</fieldset>
</form>
<p class="step1" id="login-page"><a id="login-page" href="<?= $this->plxMotor->urlRewrite('?login-page.html');?>" title="<?= plxUtils::strCheck($plxPlugin->getParam('mnuConnexion')) ?>"><?= $plxPlugin->getParam('mnuConnexion') ?></a></p>