# Initial and old maintener:
- Cyril MAGUIRE alias Jerry Wham on https://www.ecyseo.net

# Contributors :
- Thomas Inglès alias Sudwebdesign on https://sudwebdesign.free.fr/. Actual maintener.
- Users of PluXml who comment on forum https://forum.pluxml.org/discussion/3673/plugin-adhesion-pour-gerer-les-membres-dune-association/. Thank you for all your comments.